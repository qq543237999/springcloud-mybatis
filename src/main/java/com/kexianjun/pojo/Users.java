package com.kexianjun.pojo;

import java.math.BigDecimal;

public class Users {
    private Long userId;

    private BigDecimal account;

    private String address;

    private Integer bindAlipay;

    private Integer bindWechat;

    private Long createAt;

    private String idNo;

    private String insurersIds;

    private Long lastModifiedAt;

    private Integer level;

    private String mobile;

    private String modifiedBy;

    private Long parentId;

    private String parentUsr;

    private String password;

    private String phone;

    private String postalCode;

    private String realName;

    private Integer roleId;

    private String username;

    private Integer version;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public BigDecimal getAccount() {
        return account;
    }

    public void setAccount(BigDecimal account) {
        this.account = account;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public Integer getBindAlipay() {
        return bindAlipay;
    }

    public void setBindAlipay(Integer bindAlipay) {
        this.bindAlipay = bindAlipay;
    }

    public Integer getBindWechat() {
        return bindWechat;
    }

    public void setBindWechat(Integer bindWechat) {
        this.bindWechat = bindWechat;
    }

    public Long getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Long createAt) {
        this.createAt = createAt;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo == null ? null : idNo.trim();
    }

    public String getInsurersIds() {
        return insurersIds;
    }

    public void setInsurersIds(String insurersIds) {
        this.insurersIds = insurersIds == null ? null : insurersIds.trim();
    }

    public Long getLastModifiedAt() {
        return lastModifiedAt;
    }

    public void setLastModifiedAt(Long lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy == null ? null : modifiedBy.trim();
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getParentUsr() {
        return parentUsr;
    }

    public void setParentUsr(String parentUsr) {
        this.parentUsr = parentUsr == null ? null : parentUsr.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode == null ? null : postalCode.trim();
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}