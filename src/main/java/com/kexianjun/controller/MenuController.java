package com.kexianjun.controller;

import com.kexianjun.pojo.Menus;
import com.kexianjun.service.MenuService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * controller
 *
 * @author Administrator
 */
@RestController
@RequestMapping("/menu")
public class MenuController {

  @Autowired
  private MenuService service;

  @GetMapping
  public List<Menus> getList() {
    return service.getList();
  }

}
