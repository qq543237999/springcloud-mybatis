package com.kexianjun.service;

import com.kexianjun.pojo.Menus;
import java.util.List;

/**
 * @author kexianjun
 * @date 2018/11/6 10:56
 * @since JDK1.8
 */

public interface MenuService {
  public List<Menus> getList();
}
