package com.kexianjun.service.impl;

import com.kexianjun.mapper.MenusMapper;
import com.kexianjun.pojo.Menus;
import com.kexianjun.service.MenuService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author kexianjun
 * @date 2018/11/6 10:57
 * @since JDK1.8
 */
@Service
@Transactional(readOnly = true)
public class MenuserviceImpl implements MenuService {
  @Autowired
  private MenusMapper menusMapper;
  @Override public List<Menus> getList() {
    return menusMapper.selectByExample(null);
  }
}
